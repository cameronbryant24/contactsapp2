package com.example.contactsapp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;

public class list extends AppCompatActivity {
    Button btn_add, btn_sortABC, btn_sortNumber;

    ListView lv_friendsList;

    PersonAdapter adapter;

    MyContacts myContacts;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        myContacts = ((MyApp) this.getApplication()).getMyContacts();

        btn_add = findViewById(R.id.btn_add);
        btn_sortABC = findViewById(R.id.btn_sortABC);
        btn_sortNumber = findViewById(R.id.btn_sortNumber);
        lv_friendsList = findViewById(R.id.lv_listOfNames);


        adapter = new PersonAdapter(list.this, myContacts);

        lv_friendsList.setAdapter(adapter);

        Bundle incomingMessages = getIntent().getExtras();

        if (incomingMessages != null) {

            String firstName = incomingMessages.getString("first name");
            String lastName = incomingMessages.getString("last name");
            int number = Integer.parseInt(incomingMessages.getString("number"));
            String email = incomingMessages.getString("email");
            String address = incomingMessages.getString("address");
            String url = incomingMessages.getString("url");
            String businessHours = incomingMessages.getString("business hours");
            String birthDay = incomingMessages.getString("birth day");
            String description = incomingMessages.getString("description");
            int positionEdited = incomingMessages.getInt("edit");

            Person p = new Person(firstName, lastName, number, email, address, url, businessHours, birthDay, description);

            if (positionEdited > -1) {
                myContacts.getMyContactsList().remove(positionEdited);
            }
            myContacts.getMyContactsList().add(p);
            adapter.notifyDataSetChanged();

        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent( v.getContext()  ,  AddPersonForm.class );
                startActivity(i);
            }
        });

        btn_sortNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.sort(myContacts.getMyContactsList(), new Comparator<Person>() {
                    @Override
                    public int compare(Person p1, Person p2) {
                        return p1.getNumber() - p2.getNumber();
                    }
                });

                adapter.notifyDataSetChanged();
            }
        });

        btn_sortABC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.sort(myContacts.myContactsList);
                adapter.notifyDataSetChanged();
            }
        });

        lv_friendsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editPerson(position);
            }
        });

    }

    public void editPerson(int position) {

        Intent i = new Intent(getApplicationContext(), AddPersonForm.class);

        Person p = myContacts.getMyContactsList().get(position);

        i.putExtra("edit", position);
        i.putExtra("name", p.getFirstName());
        i.putExtra("number", p.getNumber());
        i.putExtra("email", p.getEmail());
        i.putExtra("address", p.getAddress());
        i.putExtra("url", p.getUrl());
        i.putExtra("business hours", p.getBusinessHours());
        i.putExtra("birthday", p.getBirthDay());
        i.putExtra("description", p.getDescription());

        startActivity(i);

    }

}