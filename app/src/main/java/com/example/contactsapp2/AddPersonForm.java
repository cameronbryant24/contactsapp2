package com.example.contactsapp2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddPersonForm extends AppCompatActivity {
    Button btn_ok, btn_cancel, btn_call, btn_text, btn_email, btn_url;
    EditText et_firstName, et_lastName, et_number, et_email, et_address, et_url, et_businessHours, et_birthDay, et_description;

    int positionToEdit = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_person);

        btn_ok = findViewById(R.id.btn_ok);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_call = findViewById(R.id.btn_call);
        btn_text = findViewById(R.id.btn_text);
        btn_email = findViewById(R.id.btn_email);
        btn_url = findViewById(R.id.btn_url);

        et_firstName = findViewById(R.id.et_firstName);
        et_lastName = findViewById(R.id.et_lastName);
        et_number = findViewById(R.id.et_number);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
        et_url = findViewById(R.id.et_url);
        et_businessHours = findViewById(R.id.et_businessHours);
        et_birthDay = findViewById(R.id.et_birthDay);
        et_description = findViewById(R.id.et_description);

        Bundle incomingIntent = getIntent().getExtras();

        if (incomingIntent != null) {
            String firstName = incomingIntent.getString("first name");
            String lastName = incomingIntent.getString("last name");
            int number = incomingIntent.getInt("number");
            String email = incomingIntent.getString("email");
            String address = incomingIntent.getString("address");
            String url = incomingIntent.getString("url");
            String businessHours = incomingIntent.getString("business hours");
            String birthDay = incomingIntent.getString("birth day");
            String description = incomingIntent.getString("description");
            positionToEdit = incomingIntent.getInt("edit");

            et_firstName.setText(firstName);
            et_lastName.setText(lastName);
            et_number.setText(Integer.toString(  number));
            et_email.setText(email);
            et_address.setText(address);
            et_url.setText(url);
            et_businessHours.setText(businessHours);
            et_birthDay.setText(birthDay);
            et_description.setText(description);

        }

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newFirstName = et_firstName.getText().toString();
                String newLastName = et_lastName.getText().toString();
                String newNumber = et_number.getText().toString();
                String newEmail = et_email.getText().toString();
                String newAddress = et_address.getText().toString();
                String newUrl = et_url.getText().toString();
                String newBusinessHours = et_businessHours.getText().toString();
                String newBirthDay = et_birthDay.getText().toString();
                String newDescription = et_description.getText().toString();

                Intent i = new Intent(v.getContext(), list.class);

                i.putExtra("edit", positionToEdit);
                i.putExtra("first name", newFirstName);
                i.putExtra("last name", newLastName);
                i.putExtra("number", newNumber);
                i.putExtra("email", newEmail);
                i.putExtra("address", newAddress);
                i.putExtra("url", newUrl);
                i.putExtra("business hours", newBusinessHours);
                i.putExtra("birth day", newBirthDay);
                i.putExtra("description", newDescription);
                startActivity(i);

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), list.class);
                startActivity(i);
            }
        });

        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String [] addresses = new String[1];
                addresses[0] = et_email.getText().toString();

                composeEmail(addresses, "Hello from Cameron");
            }
        });

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPhoneNumber(et_number.getText().toString());
            }
        });

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                composeMmsMessage(et_number.getText().toString(), "Hello I would like to talk");
            }
        });

        btn_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWebPage(et_url.getText().toString());
            }
        });

    }

    public void openWebPage(String url) {

        if (!url.startsWith("http://") || !url.startsWith("https://"))
            url = "http://" + url;
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void composeMmsMessage(String number, String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:"  + et_number));  // This ensures only SMS apps respond
        intent.putExtra("sms_body", message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void callPhoneNumber(String number) {

    }

}